<h1 align="center">Hi 👋, I'm Space Time</h1>
<h3 align="center">Welcome To My Github ~</h3>
</br>

## Github Stats 🌐:
<img align="left" src="https://github-readme-stats.vercel.app/api?username=SpaceTimee&include_all_commits=true&count_private=true&show_icons=true&icon_color=CE1D2D&text_color=718096&hide_border=true&hide_title=true" />
<img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=SpaceTimee&card_width=230&layout=compact&langs_count=10&hide_border=true&custom_title=Most Used Languages ~" />

## About Me 🤪:
**Space Time(?~?): 名Space，姓Time，啥都会一点，但又啥都不会；孩在上鞋，但又不一直都上；是独立开发者，但又不完全是；擅长C/C++与C#(Winform)混编，但又不是特别擅长**

## Navigate 🧭:
* **SpaceTime Center: Just [here](http://www.spacetimee.xyz) (开源URL: [https://github.com/SpaceTimee/SpaceTime-Center](https://github.com/SpaceTimee/SpaceTime-Center)) (主页)**

* **SpaceTime Blog: [https://blog.spacetimee.xyz](https://blog.spacetimee.xyz) (开源URL: [https://github.com/SpaceTimee/SpaceTime-Blog](https://github.com/SpaceTimee/SpaceTime-Blog)) (新博客，还在装修)**

* SpaceTime Spare Blog: [https://www.cnblogs.com/spacetime](https://www.cnblogs.com/spacetime) (备用博客，已很久未维护)

* SpaceTime DeputyBase: NULL (微信公众号，太久没用被冻结了...)

## Doing List 📒:
* 制作**Vight Note (开源URL: [https://github.com/SpaceTimee/Vight-Note](https://github.com/SpaceTimee/Vight-Note))**

## To-Do List 📕:
* Winform转WPF/UWP

* 学习Xamarin

* 制作Vight OCR

* 制作Vight Bot

* 学习使用Windows API

* 制作无限音控 (难度过高，暂时保留)

## Done List 📗:
* 一些用Scratch制作的小游戏 (弃坑4年，年久失修，大多已不能正常运行，故不在此展示)

* 一些用C++ Console制作的小东西 (开源URL: [https://github.com/SpaceTimee/SpaceTime-FormerWorks](https://github.com/SpaceTimee/SpaceTime-FormerWorks))

## Contact Me 📢:
* **邮箱: Zeus6_6@163.com (推荐)**

* Github: [https://github.com/SpaceTimee](https://github.com/SpaceTimee) (Gimme Fork or Star?)

* Bilibili: [https://space.bilibili.com/171416312](https://space.bilibili.com/171416312) (别看啦，啥都没有)

•ᴗ•
